﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace App8.Droid
{
    [Activity(Label = "App8.Android", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.myButton);

            button.Click += delegate
            {
                button.Text = string.Format("{0} clicks!", count++);
            };


            //スクロールビューを作る
            var ll1 = FindViewById<LinearLayout>(Resource.Id.ll1);
            for (int i = 0; i < 100; i++)
            {
                var tv = new TextView(this);
                tv.Text = i.ToString();
                tv.Background = Resources.GetDrawable(Resource.Drawable.border);
                tv.SetTextSize(Android.Util.ComplexUnitType.Sp, 30);

                ll1.AddView(tv,
                    new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MatchParent
                        , LinearLayout.LayoutParams.WrapContent
                        ));
            }
        }
    }
}


